%define debug_package %{nil}

Name:           hugo-extended
Version:        0.139.3
Release:        1%{?dist}
Summary:        The world’s fastest framework for building websites. Extended Edition

License:        ASL 2.0
URL:            https://gohugo.io/
Source0:        https://github.com/gohugoio/hugo/releases/download/v%{version}/hugo_extended_%{version}_Linux-64bit.tar.gz

Conflicts:      hugo

%description
The world’s fastest framework for building websites. Extended Edition

%prep
%autosetup -n %{name} -c

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/usr/bin
install -p -m 755 hugo %{buildroot}/usr/bin

%files
%license LICENSE
%doc README.md
/usr/bin/hugo

%changelog
* Mon Dec 09 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Mon Jun 10 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Tue May 28 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Tue Apr 30 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Thu Apr 11 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Sun Mar 03 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to v0.123.7

* Thu Dev 28 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.121.1

* Wed Nov 08 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.120.4

* Thu Nov 02 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.120.3

* Mon Sep 25 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.119.0

* Thu Aug 31 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.118.2

* Wed Aug 09 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.117.0

* Wed Aug 01 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.116.1

* Mon Jul 31 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.116.0

* Wed Jul 26 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.115.4

* Mon Jun 19 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.114.0

* Tue Apr 25 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.111.3

* Mon Feb 06 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.111.2

* Fri Feb 03 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.111.1

* Tue Jan 17 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.110.0

* Fri Nov 25 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.107.0

* Tue Nov 01 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.105.0

* Thu Oct 06 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.104.3

* Wed Sep 28 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.104.1

* Mon Sep 26 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 0.104.0

* Thu Sep 22 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 0.103.1

* Thu Sep 11 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Inital RPM
